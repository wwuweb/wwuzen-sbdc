<?php

$plugin = array(
  'title' => t('3x2 Grid'),
  'category' => t('Western'),
  'icon' => '3x2-grid.png',
  'theme' => '3x2-grid',
  'css' => '../../css/panel-layouts/3x2-grid.css',
  'regions' => array(
    'top-left' => t('Top Left'),
    'top-center' => t('Top Center'),
    'top-right' => t('Top Right'),
    'bottom-left' => t('Bottom Left'),
    'bottom-center' => t('Bottom Center'),
    'bottom-right' => t('Bottom Right')
  )
);
